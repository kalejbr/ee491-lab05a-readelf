//##############################################################################
//### University of Hawaii, College of Engineering
//### @brief  Lab 05a - readelf - EE 491 - Spr 2022
//###
//### @file file.c
//### @version 1.1
//###
//###
//###
//### @author Kale Beever-Riordon <kalejbr@hawaii.edu>
//### @date   23_Mar_2022
//###############################################################################

#include "stdio.h"
#include "stdlib.h"
#include "errno.h"
#include "fcntl.h"
#include "sys/stat.h"
#include "unistd.h"

off_t getFileSize(int fd){

    struct stat buf;
    //Using the errno library the errno value is saved prior to calling fstat to mitigate confusing indications in case
    // of a fault
    int errno_save = errno;

    //If no such file or directory...
    if (fstat(fd, &buf) == -1) {
        perror("readelf");
        exit(errno);
    }

    errno = errno_save;
    //Now we can allocate the correct amount of memory
    return buf.st_size;
}

//This function opens the file read-only, farms it out to another function to get the size, allocates memory, and closes the fi
void *read_file(const char *filename){
    int fd = open(filename, O_RDONLY);
    if (fd == -1){
        perror("readelf");
        exit(errno);
    }

    //To get the size we stat the puppy
    off_t size = getFileSize(fd);
    //+1 for the ending NULL bite
    void* buf = malloc(size + 1);
    if (read(fd, buf, size) != size){
        perror("readelf");
        exit(errno);
    }
    close(fd);
    return buf;
}
