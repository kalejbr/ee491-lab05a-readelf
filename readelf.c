//###############################################################################
//### University of Hawaii, College of Engineering
//### @brief  Lab 05a - readelf - EE 491 - Spr 2022
//###
//### @file readelf.c
//### @version 1.1
//###
//###
//###
//### @author Kale Beever-Riordon <kalejbr@hawaii.edu>
//### @date   23_Mar_2022
//###############################################################################

#include "stdio.h"
#include "stdlib.h"
#include "readelf.h"

void print_usage(void){
    fprintf(stdout, "Usage: readelf displays information about ELF format files\n");
}

void readElfData(const char *path){

    //Read the file, store the contents in the buffer, and close the file
    unsigned char *buf = (unsigned char *) read_file(path);
    //Using what's in the buffer, we print the header
    printHeader(buf);

    free(buf);
}

int main(int argc, char* argv[]){

    if (argc < 2){
        print_usage();
        exit(1);
    }
    for(int i = 1; i < argc; i++){
        readElfData(argv[i]);
    }
    return 0;
}
