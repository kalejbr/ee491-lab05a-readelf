//###############################################################################
//### University of Hawaii, College of Engineering
//### @brief  Lab 05a - readelf - EE 491 - Spr 2022
//###
//### @file elfheader.c
//### @version 1.1
//###
//###
//###
//### @author Kale Beever-Riordon <kalejbr@hawaii.edu>
//### @date   23_Mar_2022
//###############################################################################

#include "stdio.h"
#include "stdlib.h"
#include "readelf.h"

//A single "internal" struct is used that changes it's parameters based on whether or not we are in a 32-bit architecture
//or a 64-bit architecture
Elf_Internal_Ehdr *get_elf_header(const unsigned char *buf){
    //Calls internal header from readelf.h
    static Elf_Internal_Ehdr header;

    //We look at the first row of machine instructions
    for(int i = 0; i < EI_NIDENT; i++){
        header.ident[i] = buf[i];
    }
    //If 64 bit...
    if (buf[EI_CLASS] == CLASS_64){
        Elf64_Ehdr *elf64 = (Elf64_Ehdr *) buf;
        header.type = elf64->type;
        header.machine = elf64->machine;
        header.version = elf64->version;
        header.entry = elf64->entry;
        header.phoffset = elf64->phoffset;
        header.shoffset = elf64->shoffset;
        header.flags = elf64->flags;
        header.ehsize = elf64->ehsize;
        header.phentsize = elf64->phentsize;
        header.phnum = elf64->phnum;
        header.shentsize = elf64->shentsize;
        header.shnum = elf64->shnum;
        header.shstrndx = elf64->shstrndx;
    }else { //If 32-bit...
        Elf32_Ehdr *elf32 = (Elf32_Ehdr *) buf;
        header.type = elf32->type;
        header.machine = elf32->machine;
        header.version = elf32->version;
        header.entry = elf32->entry;
        header.phoffset = elf32->phoffset;
        header.shoffset = elf32->shoffset;
        header.flags = elf32->flags;
        header.ehsize = elf32->ehsize;
        header.phentsize = elf32->phentsize;
        header.phnum = elf32->phnum;
        header.shentsize = elf32->shentsize;
        header.shnum = elf32->shnum;
        header.shstrndx = elf32->shstrndx;
    }
    return &header;
}

//Function that returns the magic number
void printMagic(const unsigned char *ident) {
    fprintf(stdout, "   Magic:  ");
    for(int i = 0; i<EI_NIDENT; i++){
        printf(" %02x", ident[i]);
    }
    printf("\n");
}

//Function that returns the CLASS
char* getClass(const unsigned char class){
    static char buf[32];

    switch(class){
        case CLASS_NONE:
            return "none";
        case CLASS_32:
            return "ELF32";
        case CLASS_64:
            return "ELF64";
        default:
            return "UNKNOWN";
    }
    return buf;
}

//Function that returns the Encoding type
char* getEncoding(const unsigned char encoding){
    static char buf[32];

    switch (encoding){
        case DATA_NONE:
            return "None";
        case DATA_LSB:
            return "2's complement, little endian";
        case DATA_MSB:
            return "2's complement, big endian";
        default:
            return "Unknown";
    }
    return buf;
}

//Function that returns the version
char* getVersion(const unsigned char version){
    static char buf[32];

    switch (version) {
        case V_CURRENT:
            return "1 (current)";
        case V_NONE:
            return "0";
        default:
            return "unknown";
    }
    return buf;
}

//Function that returns the OS ABI
char* getOSABI(const unsigned char osabi){
    static char buf[32];

    switch (osabi) {
        case OSABI_NONE:
            return "UNIX - System V";
        case OSABI_HPUX:
            return "UNIX - HP-UX";
        case OSABI_NETBSD:
            return "UNIX - NetBSD";
        case OSABI_GNU:
            return "UNIX - GNU";
        case OSABI_SOLARIS:
            return "UNIX - Solaris";
        case OSABI_AIX:
            return "UNIX - AIX";
        case OSABI_IRIX:
            return "UNIX - IRIX";
        case OSABI_FREEBSD:
            return "UNIX - FreeBSD";
        case OSABI_TRU64:
            return "UNIX - TRU64";
        case OSABI_MODESTO:
            return "Novell - Modesto";
        case OSABI_OPENBSD:
            return "UNIX - OpenBSD";
        default:
            return "Machine dependent OS ABI";
    }
    return buf;
}

//Function that returns the Type
char* getType(uint16_t type){
    static char buf[32];
    switch (type) {
        case Type_NONE:
            return "NONE (None)";
        case Type_REL:
            return "REL (Relocatable file)";
        case Type_EXEC:
            return "EXEC (Executable file)";
        case Type_DYN:
            return "DYN (Shared object file)";
        case Type_CORE:
            return "CORE (Core file)";
        default:
            return "Unknown";
    }
    return buf;
}

//Function that returns the machine type
char* getMachine(uint16_t machine){
    static char buf[64];

    switch (machine) {
        case Mach_NONE:
            return "None";
        case Mach_M32:
            return "WE32100";
        case Mach_SPARC:
            return "Sparc";
        case Mach_386:
            return "Intel 80386";
        case Mach_68K:
            return "MC68000";
        case Mach_88K:
            return "MC88000";
        case Mach_IAMCU:
            return "Intel MCU";
        case Mach_860:
            return "Intel 80860";
        case Mach_MIPS:
            return "MIPS R3000";
        case Mach_ARM:
            return "ARM";
        case Mach_AMDGPU:
            return "AMD GPU";
        case Mach_RISC:
            return "RISC-V";
        default:
            return "unknown";
    }
    return buf;
}

//Function that prints the header of the binary/elf file
void printHeader(const unsigned char *buf){
    Elf_Internal_Ehdr *header = get_elf_header(buf);

    if (checkMagicNumber(header->ident)){
        fprintf(stderr,"Not an ELF file - magic bytes don't match.\n");
        exit(1);
    }

    fprintf(stdout, "ELF Header:\n");
    printMagic(header->ident);
    fprintf(stdout, "   Class:                             %s\n",     getClass(header->ident[EI_CLASS]));
    fprintf(stdout, "   Data:                              %s\n",     getEncoding(header->ident[EI_DATA]));
    fprintf(stdout, "   Version:                           %s\n",     getVersion(header->ident[EI_VERSION]));
    fprintf(stdout, "   OS/ABI:                            %s\n",     getOSABI(header->ident[EI_OSABI]));
    fprintf(stdout, "   ABI Version:                       %x\n",     header->ident[EI_ABI_V]);
    fprintf(stdout, "   Type:                              %s\n",     getType(header->type));
    fprintf(stdout, "   Machine:                           %s\n",     getMachine(header->machine));
    fprintf(stdout, "   Version:                           %x\n",     header->version);
    fprintf(stdout, "   Entry Point Address:               0x%llx\n", header->entry);
    fprintf(stdout, "   Start of program headers:          %llu (bytes into file)\n", header->phoffset);
    fprintf(stdout, "   Start of section headers:          %llu (bytes into file)\n", header->shoffset);
    fprintf(stdout, "   Flags:                             %x\n",     header->flags);
    fprintf(stdout, "   Size of this header:               %d\n",     header->ehsize);
    fprintf(stdout, "   Size of Program headers:           %d\n",     header->phentsize);
    fprintf(stdout, "   Number of Program headers:         %d\n",     header->phnum);
    fprintf(stdout, "   Size of Section Headers:           %d\n",     header->shentsize);
    fprintf(stdout, "   Number of Section Headers:         %d\n",     header->shnum);
    fprintf(stdout, "   Section header string table index: %d\n",     header->shstrndx);
}



