//###############################################################################
//### University of Hawaii, College of Engineering
//### @brief  Lab 05a - readelf - EE 491 - Spr 2022
//###
//### @file readelf.h
//### @version 1.1
//###
//###
//###
//### @author Kale Beever-Riordon <kalejbr@hawaii.edu>
//### @date   23_Mar_2022
//###############################################################################

#include "stdint.h"

#define EI_MAG0         0
#define EI_MAG1         1
#define EI_MAG2         2
#define EI_MAG3         3
#define EI_CLASS        4
#define EI_DATA         5
#define EI_VERSION      6
#define EI_OSABI        7
#define EI_ABI_V        8
#define EI_NIDENT       16

#define MAG0            0x7f
#define MAG1            'E'
#define MAG2            'L'
#define MAG3            'F'

#define CLASS_NONE      0
#define CLASS_32        1
#define CLASS_64        2

#define DATA_NONE       0
#define DATA_LSB        1
#define DATA_MSB        2

//structs for the 32-bit and 64 bit architectures
typedef struct {
    unsigned char ident[EI_NIDENT];     /* Magic number and other info */
    uint16_t      type;                 /* Object file type */
    uint16_t      machine;              /* Architecture */
    uint32_t      version;              /* Object file version */
    uint32_t      entry;                /* Entry point virtual address */
    uint32_t      phoffset;             /* Program header table file offset */
    uint32_t      shoffset;             /* Section header table file offset */
    uint32_t      flags;                /* Processor-specific flags */
    uint16_t      ehsize;               /* ELF header size in bytes */
    uint16_t      phentsize;            /* Program header table entry size */
    uint16_t      phnum;                /* Program header table entry count */
    uint16_t      shentsize;            /* Section header table entry size */
    uint16_t      shnum;                /* Section header table entry count */
    uint16_t      shstrndx;             /* Section header string table index */
} Elf32_Ehdr;

typedef struct {
    unsigned char ident[EI_NIDENT];      /* Magic number and other info */
    uint16_t      type;                  /* Object file type */
    uint16_t      machine;               /* Architecture */
    uint32_t      version;               /* Object file version */
    uint64_t      entry;                 /* Entry point virtual address */
    uint64_t      phoffset;              /* Program header table file offset */
    uint64_t      shoffset;              /* Section header table file offset */
    uint32_t      flags;                 /* Processor-specific flags */
    uint16_t      ehsize;                /* ELF header size in bytes */
    uint16_t      phentsize;             /* Program header table entry size */
    uint16_t      phnum;                 /* Program header table entry count */
    uint16_t      shentsize;             /* Section header table entry size */
    uint16_t      shnum;                 /* Section header table entry count */
    uint16_t      shstrndx;              /* Section header string table index */
} Elf64_Ehdr;

typedef struct {
    unsigned char ident[EI_NIDENT];      /* Magic number and other info */
    uint16_t      type;                  /* Object file type */
    uint16_t      machine;               /* Architecture */
    uint32_t      version;               /* Object file version */
    uint64_t      entry;                 /* Entry point virtual address */
    uint64_t      phoffset;              /* Program header table file offset */
    uint64_t      shoffset;              /* Section header table file offset */
    uint32_t      flags;                 /* Processor-specific flags */
    uint16_t      ehsize;                /* ELF header size in bytes */
    uint16_t      phentsize;             /* Program header table entry size */
    uint16_t      phnum;                 /* Program header table entry count */
    uint16_t      shentsize;             /* Section header table entry size */
    uint16_t      shnum;                 /* Section header table entry count */
    uint16_t      shstrndx;              /* Section header string table index */
} Elf_Internal_Ehdr;

// OS/ABI
#define OSABI_NONE       0
#define OSABI_HPUX       1
#define OSABI_NETBSD     2
#define OSABI_GNU        3
#define OSABI_SOLARIS    6
#define OSABI_AIX        7
#define OSABI_IRIX       8
#define OSABI_FREEBSD    9
#define OSABI_TRU64      10
#define OSABI_MODESTO    11
#define OSABI_OPENBSD    12

// File type
#define Type_NONE   0
#define Type_REL    1
#define Type_EXEC   2
#define Type_DYN    3
#define Type_CORE   4

// Machine Architecture
#define Mach_NONE   0
#define Mach_M32    1
#define Mach_SPARC  2
#define Mach_386    3
#define Mach_68K    4
#define Mach_88K    5
#define Mach_IAMCU  6
#define Mach_860    7
#define Mach_MIPS   8
#define Mach_ARM    40
#define Mach_AMDGPU 224
#define Mach_RISC   243

// Object file version
#define V_NONE      0
#define V_CURRENT   1

//
Elf_Internal_Ehdr *get_elf_header(const unsigned char *);
void *read_file(const char *);
int checkMagicNumber( const unsigned char *);
void printHeader(const unsigned char *);


