//###############################################################################
//### University of Hawaii, College of Engineering
//### @brief  Lab 05a - readelf - EE 491 - Spr 2022
//###
//### @file elf.c
//### @version 1.1
//###
//###
//###
//### @author Kale Beever-Riordon <kalejbr@hawaii.edu>
//### @date   23_Mar_2022
//###############################################################################

#include "readelf.h"

int checkMagicNumber( const unsigned char *ident) {
    return ident[EI_MAG0] != MAG0 ||
           ident[EI_MAG1] != MAG1 ||
           ident[EI_MAG2] != MAG2 ||
           ident[EI_MAG3] != MAG3;
}
