###############################################################################
### University of Hawaii, College of Engineering
### @brief  Lab 05a - readelf - EE 491 - Spr 2022
###
### @file Makefile
### @version 1.1
###
###
###
### @author Kale Beever-Riordon <kalejbr@hawaii.edu>
### @date   23_Mar_2022
###############################################################################

CC			= gcc
CFLAGS 		= -Wall
LDFLAGS 	=
OBJFILES 	= elfheader.o readelf.o file.o elf.o
TARGET 		= test

all: $(TARGET)

$(TARGET): $(OBJFILES)
	$(CC) $(CFLAGS) -o $(TARGET) $(OBJFILES) $(LDFLAGS)

clean:
	rm -f $(OBJFILES) $(TARGET)

